const path = require('path');
const ip = require("ip");
const express = require('express');
const app = express();
const sqlite = require("sqlite");
const bodyParser = require("body-parser");

let dbJukebox = null;

app.use(express.static("app")); //middleware

app.use(bodyParser.urlencoded({
  extended: true
}));

app.get("/", async (req, res) => {
  const today_visits = await dbJukebox.get("SELECT COUNT(*) AS num_visitors_today FROM visitors WHERE visit_date = DATE('now')");
  const num_visitors_today = today_visits.num_visitors_today;

  const visitors = dbJukebox.all("SELECT * FROM visitors ORDER BY id DESC");

  res.render("index", {
    num_visitors_today,
    visitors
  });
});

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/app/index.html'));
});


app.post("/", async (req, res) => {

  dbJukebox.run("INSERT INTO visitors ( visit_date ) VALUES ( $DATE('now') )", {
  });

  res.redirect("/");
});

sqlite.open("./visitors.sqlite").then(db => {
  dbJukebox = db;
  app.listen(9080, () => {
 	console.log("Jukebox is running on -> " + ip.address() + ":9080");
  });
});