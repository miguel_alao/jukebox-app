let youtube_player;
let mp3_player = document. getElementById('mp3-player');
const list = document.getElementById("MusicList"); 
const JukeBox1 = new JukeBox();

//document.getElementById("Volume").innerText = JukeBox1.volume * 100;

console.log(JukeBox1);

// Get files from system and put them on JukeBox1
FileLoader.onchange = function(){
    for (let i = 0; i < this.files.length; i++)
    {
        const file = this.files[i];
        console.log(file);

        // Simple API - will fetch all tags
        jsmediatags.read(file, {
            onSuccess: function(tag) {
                const NewSong = new Mp3Song(file, tag.tags, mp3_player);
                JukeBox1.addMusic(NewSong);
                addToList(NewSong);
            },
            onError: function(error) {
                console.log(':(', error.type, error.info);
            }
        });
    }
};


// add youtube music by id, artist, music name

function addYoutube(song) {
    console.log (song);
    const NewSong = new YoutubeSong(song, youtube_player);
    JukeBox1.addMusic(NewSong);
    addToList(NewSong);
    };

// Fill the music list
function addToList(item) {
    let option = document.createElement("option");
    list.appendChild(option);
    list.size = list.size + 1;
    
    let text = document.createTextNode(item.artist + ' - ' + item.title);
    option.addEventListener('dblclick', function(){
        console.log(option.index);
        JukeBox1.gotoSong(option.index)
    });
    option.appendChild(text);
};

function onYouTubeIframeAPIReady(){
    console.log("creating youtube");
    youtube_player = new YT.Player('youtube-player', {
        height: '0',
        width: '0',
        playerVars: {
            autoplay: '0',
            loop: '0',
        },
        events: {
            'onReady': function(e){

                console.log("youtube ready");
            }
        }
    });
};

document.getElementById("rewindBtn").addEventListener("click", function(){JukeBox1.rewind()});
document.getElementById("stopBtn").addEventListener("click", function(){JukeBox1.stop()});
document.getElementById("playBtn").addEventListener("click", function(){JukeBox1.play()});
document.getElementById("pauseBtn").addEventListener("click", function(){JukeBox1.pause()});
document.getElementById("forwardBtn").addEventListener("click", function(){JukeBox1.forward()});
document.getElementById("volDnBtn").addEventListener("click", function(){
    JukeBox1.volDn(); 
//    document.getElementById("Volume").innerText = JukeBox1.volume * 100;
});
document.getElementById("volUpBtn").addEventListener("click", function(){
    JukeBox1.volUp();
//    document.getElementById("Volume").innerHTML = JukeBox1.volume * 100;
});

document.getElementById("Shuffle").onclick = function() {
    JukeBox1.Shuffle = document.getElementById("Shuffle").checked;
};
document.getElementById("trash").addEventListener("click", function(){
    JukeBox1.delMusic(list.selectedIndex);
    list.remove(list.selectedIndex);
});
document.getElementById("addYoutube").addEventListener("click", function(){
    $('#youtubeModal').modal('hide');
    const song = {
        id: $("#songId").val(),
        artist: $("#songArtist").val(),
        title: $("#songTitle").val()
    }

    const NewSong = new YoutubeSong(song, youtube_player);
    JukeBox1.addMusic(NewSong);
    addToList(NewSong);
});
