class Mp3Song{
	constructor(file, tags, player){
		this.player = player;
		this.artist = tags.artist;
		this.title = tags.title;
		this.album = tags.album;
		this.picture = tags.picture;
		this.audio = new Audio(file);
		this.currentTime = 0;
		this.url = URL.createObjectURL(file); 
	}

	play(){
		this.player.src = this.url; 
		this.player.play();
		this.player.currentTime = this.currentTime;
	}

	pause(){
		this.currentTime = this.player.currentTime;
		this.player.pause();
	}

	stop(){
		this.player.pause();
		this.currentTime = 0;	
	}

};

