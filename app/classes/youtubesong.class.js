class YoutubeSong{
	constructor(music, player){
		this.id = music.id;
		this.artist = music.artist;
		this.title = music.title;
		this.player = player;
	}

	play(vol){
		this.player.setVolume(vol * 100);
		if (this.player.getPlayerState() == 2){
			this.player.playVideo();
		} else {
			this.player.loadVideoById({
		    	'videoId': this.id,                        
		    	'suggestedQuality': 'medium'
			});
			this.player.playVideo();
		}	
	}

	pause(){
		this.player.pauseVideo();
	}

	stop(){
		this.player.stopVideo();
		this.player.clearVideo();	
	}

	setVolume(vol){
		this.player.setVolume(vol * 100);
	}

}



//g_player.loadVideoById({
//    'videoId': 'rjLEsYbrHyQ',                        
//    'suggestedQuality': 'medium'
//});
//g_player.playVideo();