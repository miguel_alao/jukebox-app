class JukeBox{
	constructor(){
		this.musicList = [];					
		this.musicIndex = 0;
		this.volume = 0.5;
		this.Shuffle = false;
		this.playingMusic = false;
	}

	addMusic(song){
		this.musicList.push(song);
	}

	delMusic(index){
		this.musicList.splice(index, 1);
	// TODO if musicindex is equal to music that is playing and the music that is playing is to be deleted, do STOP.
	}

	play(){
		this.playingMusic = true;
		this.musicList[this.musicIndex].play(this.volume);
	}

	pause(){
		this.playingMusic = false;
		this.musicList[this.musicIndex].pause();
	}

	stop(){
		this.playingMusic = false;
		this.musicList[this.musicIndex].stop();
	}

	gotoSong(index){
		this.stop();
		this.musicIndex = index;
		this.play();
	}

	forward(){
		this.stop();
	    if (this.Shuffle === true){
	    	this.musicIndex = Math.floor((Math.random() * this.musicList.length));
	    	console.log(this.musicIndex);
	    } else {
			this.musicIndex++;
			if (this.musicIndex >= this.musicList.length){
				this.musicIndex = 0;
			}
			document.getElementById("Music").selectedIndex = this.musicIndex;
		}
		this.play();		
	}

	rewind(){
		this.stop();	
		if (this.musicIndex > 0){
			this.musicIndex--;	
		}
		document.getElementById("Music").selectedIndex = this.musicIndex;
		this.play();
	}
	//check rewind method, as it won´t go back from the first song.

	volUp(){
		let vol = this.volume;
		if (vol < 1){
			vol = Math.round((vol + 0.05) * 100) / 100;
			mp3_player.volume = vol;
			youtube_player.setVolume(vol * 100);
			this.volume = vol;
		}
	}

	volDn(){
		let vol = this.volume;
		if (vol > 0){
			vol = Math.round((vol - 0.05) * 100) / 100;
			mp3_player.volume = vol;
			youtube_player.setVolume(vol * 100);
			this.volume = vol;
		}
	}
}	
