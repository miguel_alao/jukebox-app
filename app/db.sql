CREATE TABLE visitors (
  id INTEGER PRIMARY KEY,
  visit_date TEXT NOT NULL
);
